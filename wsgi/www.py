"""
WSGI config for production deploy.
"""

import os
from django.core.wsgi import get_wsgi_application
from raven import Client
from raven.middleware import Sentry

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

application = get_wsgi_application()

from django.conf import settings

application = Sentry(
    application,
    Client(settings.RAVEN_CONFIG["dsn"])
)
