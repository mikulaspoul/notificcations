## Notifi[CC]ations

This is an small app that checks for new videos from Michael Aranda's channel WhatAmIDoingRightNow and then checks them if the subtitles are added yet. Once they are the app sends notifications to signed up users.

# Server requirements:

  * Python 3.6
  * Database (production runs of PostgreSQL, probably would run on SQLite without significant performance issues)
  * Cron
  * Nginx/Apache

# How to run locally:

  * Create and activate a virtual enviroment
  * Run `pip install -r requirements.txt` to install requirements
  * Create a file `settings/local_settings.py` with settings, example is in gile `settings/local_settings.py`:
    * Pay attention specifically to `DATABASES`, email settings (SMTP) and all the settings from `settings/app.py`
  * Run `python manage.py migrate` - Creates database schema and runs migrations
  * Run `python manage.py runserver` - Runs a development server at `127.0.0.1:8000`

# Cron

Notifi[CC]ations uses the library `django_cron` for its cron usage, so the only entry in `crontab` needs to be the one bellow - all jobs are run from that command.

```
* * * * * /<path_to_python>/python /<path_to_project>/manage.py runcrons
```

# Static

On production the static files should be handled by Apache or Nginx. An example from production apache settings

```
    Alias /static/ /var/www/hosts/bettersubbox.com/notificcations/notificcations/collected_static/

    <Directory /var/www/hosts/bettersubbox.com/notificcations/notificcations/collected_static/>
            Order Allow,Deny
            Allow from all
            Options FollowSymlinks
    </Directory>
```
