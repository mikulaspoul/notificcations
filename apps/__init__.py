# encoding=utf-8
from __future__ import unicode_literals, print_function

from raven.contrib.django.raven_compat.models import client as clnt
from raven.contrib.django.client import DjangoClient

client = clnt  # type: DjangoClient
