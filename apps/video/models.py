# encoding=utf-8
from __future__ import unicode_literals, print_function

from datetime import date
from django.db import models
from django.utils.dateparse import parse_time
from django.utils.translation import ugettext_lazy as _


class Video(models.Model):
    """ Database representation of video. """

    youtube_id = models.CharField(max_length=50, verbose_name=_("Youtube id"), unique=True)
    title = models.CharField(max_length=255, verbose_name=_("Title"), blank=True, default="")
    description = models.TextField(verbose_name=_("Description"), blank=True, default="")
    thumbnail = models.CharField(verbose_name=_("Thumbnail"), blank=True, default="", max_length=255)
    captions = models.BooleanField(verbose_name=_("Captions"), default=False)
    notification_sent = models.BooleanField(verbose_name=_("Notifications sent"), default=False)
    duration = models.CharField(max_length=10, verbose_name=_("Duration"), default="", blank=True, null=True)
    published = models.DateTimeField(verbose_name=_("Published"), blank=True, null=True)
    captions_added = models.DateTimeField(verbose_name=_("Captions added"), blank=True, null=True)

    def __str__(self):
        return "{} - {}".format(self.youtube_id, self.title)

    class Meta:
        verbose_name = _("Video")
        verbose_name_plural = _("Videos")
        get_latest_by = "published"

    def youtube_link(self) -> str:
        return "http://youtube.com/watch?v=" + self.youtube_id

    def duration_date(self) -> date:
        return parse_time(self.duration)
