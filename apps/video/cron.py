# encoding=utf-8
from __future__ import unicode_literals, print_function

import logging

import requests
from django.db import IntegrityError
from django.utils import timezone
from django.utils.dateparse import parse_duration
from django_cron import CronJobBase, Schedule
from django.conf import settings
from typing import Dict, Any

from apps import client
from apps.video.models import Video
from apps.video.utils import parse_published, parse_thumbnail, parse_captions


class UpdateVideosJob(CronJobBase):
    schedule = Schedule(run_every_mins=10, retry_after_failure_mins=10)
    code = "video.update_videos"

    def do(self):
        unloaded_videos = list(Video.objects.filter(captions=False).values_list("youtube_id", flat=True))

        while len(unloaded_videos) > 0:  # while there are still some ids left

            ids_string = []
            # get fifty ids at a time to minimize requests (50 is max)
            while len(ids_string) < 50 and len(unloaded_videos) > 0:
                ids_string.append(unloaded_videos.pop())

            ids_string = ",".join(ids_string)

            try:
                response = requests.get(
                    'https://www.googleapis.com/youtube/v3/videos',
                    params={
                        'key': settings.YOUTUBE_API_KEY,
                        'id': ids_string,
                        'part': 'snippet,contentDetails'
                    }
                )
            except:
                client.captureException()
                raise

            data = response.json()

            for video_data in data['items']:
                video = Video.objects.get(youtube_id=video_data["id"])

                video.description = video_data['snippet']['description']
                video.thumbnail = parse_thumbnail(video_data['snippet']['thumbnails'])
                captions = parse_captions(video_data['contentDetails']['caption'])
                if video.captions != captions and captions:
                    video.captions_added = timezone.now()
                video.captions = captions
                video.title = video_data['snippet']['title']
                video.duration = parse_duration(video_data['contentDetails']['duration'])
                video.published = parse_published(video_data['snippet']['publishedAt'])
                video.save()


class LoadNewVideosJob(CronJobBase):
    schedule = Schedule(run_every_mins=10, retry_after_failure_mins=10)
    code = "video.load_new_videos"

    def treat_data(self, data):
        # type: (Dict[str, Any]) -> bool
        """ Parses the data in YouTube response
        :param data: dictionary
        :return: should the main loop continue
        """
        if 'items' not in data:
            client.captureMessage("LoadNewVideosJob failed, items not in data",
                                  extra={
                                      "response": data,
                                  },
                                  level=logging.WARNING,
                                  tags={"youtube": "new_videos"})

            return False

        videos = data['items']

        for video in videos:
            # only save basic stuff into database
            if video['id']['kind'] == 'youtube#video':
                try:
                    new_vid = Video(
                        youtube_id=video['id']['videoId'],
                        published=parse_published(video['snippet']['publishedAt']),
                        title=video['snippet']['title'],
                        captions=False,
                    )
                    new_vid.save()
                except IntegrityError:
                    # video id exists in database, no futher videos should be further
                    return False
        return True

    def do(self):
        response = None
        try:
            response = requests.get(
                'https://www.googleapis.com/youtube/v3/search',
                params={
                    'key': settings.YOUTUBE_API_KEY,
                    'channelId': settings.YOUTUBE_CHANNEL_ID,
                    'part': 'id,snippet',
                    'maxResults': 10,  # that should be enough to include all the newest videos
                    'order': 'date',
                    'type': 'video'
                }
            )
        except (requests.exceptions.ChunkedEncodingError, requests.exceptions.ConnectionError,
                requests.exceptions.SSLError) as e:
            client.captureMessage("LoadNewVideosJob failed: {}".format(e),
                                  level=logging.WARNING,
                                  extra={"response": response},
                                  tags={"youtube": "new_videos"})
        except:
            client.captureException()
            return

        data = response.json()

        # only basic details are saved, other details are added later
        result = self.treat_data(data)

        # in case 10 was not enough, lets load more
        while 'nextPageToken' in data and result:
            try:
                response = requests.get(
                    'https://www.googleapis.com/youtube/v3/search',
                    params={
                        'key': settings.YOUTUBE_API_KEY,
                        'channelId': settings.YOUTUBE_CHANNEL_ID,
                        'part': 'id,snippet',
                        'maxResults': 10,
                        'order': 'date',
                        'type': 'video',
                        'pageToken': data['nextPageToken']
                    }
                )
            except (requests.exceptions.ChunkedEncodingError, requests.exceptions.ConnectionError,
                    requests.exceptions.SSLError) as e:
                client.captureMessage("LoadNewVideosJob failed: {}".format(e),
                                      level=logging.WARNING,
                                      extra={"response": response},
                                      tags={"youtube": "new_videos"})
            except:
                client.captureException()
                return

            data = response.json()

            result = self.treat_data(data)
            if not result:
                break
