# encoding=utf-8
from __future__ import unicode_literals, print_function

from datetime import datetime
from typing import Optional, Dict

import dateutil.parser


def parse_thumbnail(dictionary: Dict[str, Dict[str, str]]) -> Optional[str]:
    """ Gets the best thumbnail available.
    """
    sizes = ['maxres', 'standart', 'high', 'medium', 'default']

    for size in sizes:
        try:
            return dictionary[size]['url']
        except KeyError:
            pass


def parse_captions(string: str) -> bool:
    """ Parses captions availability
    :param string: "true" or "false"
    :return: bool
    """
    if string == "true":
        return True
    else:
        return False


def parse_published(string: str) -> datetime:
    return dateutil.parser.parse(string)
