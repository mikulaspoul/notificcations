# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.http import urlsafe_base64_decode
from django.views.generic import FormView
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _

from apps.mailing_list.forms import MailingListForm
from apps.mailing_list.models import MailingList
from apps.video.models import Video


class IndexView(FormView):
    template_name = "index.html"
    form_class = MailingListForm

    def form_valid(self, form):
        form.save()

        messages.success(self.request, _("You were subscribed."))

        return HttpResponseRedirect(reverse("index"))

    def get_context_data(self, **kwargs):
        data = super(IndexView, self).get_context_data(**kwargs)

        data["latest_captioned"] = Video.objects.filter(captions=True).order_by("-captions_added")[:10]
        data["analytics_code"] = settings.ANALYTICS_CODE

        return data


class UnsubscribeView(TemplateView):
    def get(self, request, **kwargs):
        email = urlsafe_base64_decode(kwargs["email"])

        try:
            email = MailingList.objects.get(email=email)
        except ObjectDoesNotExist:
            messages.success(request, _("Your subscription was cancelled."))
            return HttpResponseRedirect(reverse("index"))

        email.delete()

        messages.success(request, _("Your subscription was cancelled."))

        return HttpResponseRedirect(reverse("index"))
