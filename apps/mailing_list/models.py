# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.core.mail import EmailMultiAlternatives
from django.urls import reverse
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings


class MailingList(models.Model):
    email = models.EmailField(verbose_name=_("E-mail"))

    class Meta:
        verbose_name = _("MailingList")
        verbose_name_plural = _("MailingLists")

    def get_unsubscribe_link(self):
        return self.get_link("unsubscribe")

    def get_link(self, link_type):
        # type: (unicode) -> str
        if link_type == "unsubscribe":
            return settings.SITE_URL[:-1] + reverse("mailing-list-unsubscribe",
                                                    args=[urlsafe_base64_encode(bytes(self.email, "utf8"))])
        else:
            raise ValueError(_("Invalid link type"))

    def send_email(self, subject, plain_text, html_text):
        eml = EmailMultiAlternatives(subject, plain_text,
                                     headers={"ListUnsubscribe": "<{}>".format(self.get_unsubscribe_link())})
        eml.attach_alternative(html_text, "text/html")
        eml.to = [self.email]
        eml.from_email = settings.FROM_EMAIL
        eml.send(fail_silently=False)
