# encoding=utf-8
from __future__ import unicode_literals, print_function

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from apps.mailing_list.models import MailingList


class MailingListForm(forms.ModelForm):
    class Meta:
        model = MailingList
        fields = ["email"]

    def clean_email(self):
        email = self.cleaned_data["email"]

        if email and MailingList.objects.filter(email=email).exists():
            raise ValidationError(_("Email is already subscribed."))

        return email
