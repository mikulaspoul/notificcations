# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.conf import settings
from django.db import transaction
from django.template.loader import render_to_string
from django_cron import CronJobBase, Schedule

from apps.mailing_list.models import MailingList
from apps.video.models import Video


class SendMailingJob(CronJobBase):
    schedule = Schedule(run_every_mins=60)
    code = "mailing_list.send_mails"

    def do(self):
        with transaction.atomic():
            videos = list(Video.objects.filter(captions=True, notification_sent=False))
            Video.objects.filter(captions=True, notification_sent=False).update(notification_sent=True)

        if not len(videos):
            return

        for email in MailingList.objects.all():  # type: MailingList
            context = {
                "email": email,
                "videos": videos,
                "site_url": settings.SITE_URL[:-1]
            }

            plain_text = render_to_string("email_templates/mailing_list/email.txt", context)
            html_text = render_to_string("email_templates/mailing_list/email.html", context)

            email.send_email("Notifi[CC]ations: New Captioned Videos", plain_text, html_text)
