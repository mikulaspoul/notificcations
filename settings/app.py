# encoding=utf-8
"""
Settings needed to run the app. It can run without `RAVEN_CONFIG` or `ANALYTICS_CODE`
"""
from __future__ import unicode_literals, print_function

FROM_EMAIL = "no-reply@domain.example"
SITE_URL = "http://127.0.0.1:8000/"
YOUTUBE_API_KEY = None
YOUTUBE_CHANNEL_ID = "UCcm33l460hvUiKWztuMYHHg"  # Michaels Channel ID

RAVEN_CONFIG = {
    'dsn': None,
}

ANALYTICS_CODE = None
