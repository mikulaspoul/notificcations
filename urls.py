""" URL Configuration
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

from apps.mailing_list.views import IndexView, UnsubscribeView

urlpatterns = [
    url(r'^$', IndexView.as_view(), name="index"),
    url(r'^cancel/(?P<email>[a-zA-Z0-9\-_]+)/', UnsubscribeView.as_view(), name="mailing-list-unsubscribe"),
]

if settings.DEBUG:  # static is handled with apache on production
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
